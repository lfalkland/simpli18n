package com.lfalkland.simpli18n;

/**
 * Base reader implementation.
 */
public interface I18nDocumentReader {
	
	/**
	 * Read a {@link I18nDocument}.
	 * @return a {@link I18nDocument}
	 */
	public I18nDocument read();

}
