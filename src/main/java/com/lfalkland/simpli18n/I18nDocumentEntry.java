package com.lfalkland.simpli18n;

/**
 * A I18nDocumentEntry belongs to a {@link I18nDocument}.
 * Representation of the multiple {@link LocalizedString} values that belongs to an entry.
 */
public interface I18nDocumentEntry {
	
	/** 
	 * @return the entry key
	 */
	public String getKey();
	
	/**
	 * @return the {@link LocalizedString}
	 */
	public LocalizedString getLocalizedString();

}
