package com.lfalkland.simpli18n;

import java.util.List;

/**
 * Representation of a I18n document.
 */
public interface I18nDocument {
	
	/**
	 * @return the name of the document
	 */
	public String getName();

	/**
	 * Get a {@link I18nDocumentEntry} from the provided key.
	 * @param key
	 * @return the {@link I18nDocumentEntry} or null if not found
	 */
	public I18nDocumentEntry getEntry(String key);
	
	/**
	 * @return the entries of the given document
	 */
	public List<I18nDocumentEntry> getEntries();

}
