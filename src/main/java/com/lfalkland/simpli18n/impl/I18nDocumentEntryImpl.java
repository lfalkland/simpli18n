package com.lfalkland.simpli18n.impl;

import com.lfalkland.simpli18n.I18nDocumentEntry;
import com.lfalkland.simpli18n.LocalizedString;

class I18nDocumentEntryImpl implements I18nDocumentEntry {
	
	private String mKey;
	private LocalizedString mLocalizedString;
	
	public I18nDocumentEntryImpl(String key, LocalizedString localizedString) {
		mKey = key;
		mLocalizedString = localizedString;
	}

	@Override
	public String getKey() {
		return mKey;
	}

	@Override
	public LocalizedString getLocalizedString() {
		return mLocalizedString;
	}

}
