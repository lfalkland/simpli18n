package com.lfalkland.simpli18n.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.lfalkland.simpli18n.I18nDocument;
import com.lfalkland.simpli18n.I18nDocumentEntry;

class I18nDocumentImpl implements I18nDocument {
	
	private String mName;
	private Map<String, I18nDocumentEntry> mMapEntries;
	private List<I18nDocumentEntry> mEntries;

	public I18nDocumentImpl(String name, Map<String, I18nDocumentEntry> entries) {
		mName = name;
		mMapEntries = entries;
	}
	
	@Override
	public String getName() {
		return mName;
	}
	
	@Override
	public I18nDocumentEntry getEntry(String key) {
		return mMapEntries.get(key);
	}
	
	@Override
	public List<I18nDocumentEntry> getEntries() {
		if(mEntries == null) {
			mEntries = new ArrayList<>(mMapEntries.values());
		}
		return mEntries;
	}

}
