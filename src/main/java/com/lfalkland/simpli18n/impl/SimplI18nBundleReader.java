package com.lfalkland.simpli18n.impl;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.validation.constraints.NotNull;

import com.lfalkland.simpli18n.I18nDocument;
import com.lfalkland.simpli18n.I18nDocumentEntry;
import com.lfalkland.simpli18n.I18nDocumentReader;
import com.lfalkland.simpli18n.LocalizedString;

public final class SimplI18nBundleReader implements I18nDocumentReader {
	
	private List<ResourceBundle> mResourceBundles;
	private Map<String, I18nDocumentEntry> mValues;
	private String mName;
	
	public SimplI18nBundleReader(String bundleName) {
		this(bundleName, new ArrayList<>(), SimplI18nBundleReader.class.getClassLoader());
	}
	
	public SimplI18nBundleReader(@NotNull String bundleName, @NotNull List<Locale> locales) {
		this(bundleName, locales, SimplI18nBundleReader.class.getClassLoader());
	}
	
	public SimplI18nBundleReader(@NotNull String bundleName, ClassLoader classLoader) {
		this(bundleName, new ArrayList<>(), classLoader);
	}

	public SimplI18nBundleReader(@NotNull String bundleName, @NotNull List<Locale> locales, @NotNull ClassLoader classLoader) {
		
		if(bundleName.trim().isEmpty()) {
			throw new IllegalArgumentException("You must provide a bundle name.");
		}
		
		if(locales.isEmpty()) {
			locales.add(Locale.getDefault());
		}
		
		mResourceBundles = new ArrayList<>();
		
		for(Locale locale : locales) {
			mResourceBundles.add(ResourceBundle.getBundle(bundleName, locale, classLoader));
		}
		
		mValues = new HashMap<>();
	}

	@Override
	public I18nDocument read() {
		
		for(ResourceBundle resourceBundle : mResourceBundles) {
			if(mName == null) {
				mName = resourceBundle.getBaseBundleName();
			}
			process(resourceBundle);
		}

		return new I18nDocumentImpl(mName, mValues);
		
	}
	
	private void process(ResourceBundle resourceBundle) {
		
		Enumeration<String> keys = resourceBundle.getKeys();
		Locale locale = resourceBundle.getLocale();
		
		String key;
		String value;
		I18nDocumentEntry documentEntry;
		
		while(keys.hasMoreElements()) {
			key = keys.nextElement();
			if(key.trim().isEmpty()) {
				continue;
			}
			value = resourceBundle.getString(key);
			documentEntry = mValues.getOrDefault(key, new I18nDocumentEntryImpl(key, new LocalizedString()));
			documentEntry.getLocalizedString().putValue(locale, value);
			mValues.put(key, documentEntry);
		}
		
	}

}
