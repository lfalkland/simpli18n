package com.lfalkland.simpli18n;

/**
 * Base writer implementation.
 */
public interface I18nDocumentWriter {

	/**
	 * Write a {@link I18nDocument}.
	 * @param document
	 */
	public void write(I18nDocument document);

}
