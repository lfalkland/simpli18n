package com.lfalkland.simpli18n;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.validation.constraints.NotNull;

/**
 * A Class that allows to have relationship Locale/String value.
 */
public final class LocalizedString {
	
	private Map<Locale, String> mEntries;
	
	public LocalizedString() {
		mEntries = new HashMap<>();
	}
	
	/**
	 * Indicates whether or not the Locale is defined.
	 * @param locale
	 * @return the boolean result
	 */
	public boolean exists(@NotNull Locale locale){
		return mEntries.containsKey(locale);
	}
	
	/**
	 * Get the value that belongs to the {@link Locale#getDefault()}.
	 * @return
	 */
	public String get() {
		return get(Locale.getDefault());
	}
	
	/**
	 * Get the value that belongs to the {@link Local} provided.
	 * @param locale
	 * @return
	 */
	public String get(Locale locale) {
		return get(locale, null);
	}
	
	/**
	 * Get the value that belongs to the {@link Local} provided.
	 * <br />
	 * This method allows a default value if the Local is not defined.
	 * <br />
	 * If <i>en</i> Locale is defined but <i>en_CA</i> is provided and not defined, then the value for <i>en</i> is returned.
	 * @param locale
	 * @return the value found or the default value if not found
	 */
	public String get(@NotNull Locale locale, String defaultValue) {
		String result = mEntries.getOrDefault(locale, defaultValue);
		if(result == null) {
			//We fall back
			for(Map.Entry<Locale, String> entry : mEntries.entrySet()) {
				if(entry.getKey().getLanguage().equals(locale.getLanguage())) {
					result = entry.getValue();
					break;
				}
			}
		}
		return result;
	}
	
	/**
	 * Put an entry to the given instance.
	 * @param locale
	 * @param value
	 */
	public void putValue(@NotNull Locale locale, String value) {
		mEntries.put(locale, value);
	}
	
	/**
	 * Remove all values.
	 */
	public void clear() {
		mEntries.clear();
	}
	
	/**
	 * Remove the entry that belongs to the {@link Locale} provided.
	 * @param locale
	 */
	public void remove(@NotNull Locale locale) {
		mEntries.remove(locale);
	}
	
	/**
	 * @return all the values
	 */
	public Set<Entry<Locale, String>> values() {
		return mEntries.entrySet();
	}
	
}
