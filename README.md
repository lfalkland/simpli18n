# SimplI18n

SimplI18n is a Java library that gives you a better representation than ResourceBundle in order to deal with internationalization.

A group of ResourceBundle (ex.: ``strings_en.properties``, ``strings_fr.properties``...) is represented by a I18nDocument instance.


Every entry key=value defined in a ResourceBundle is represented by a I18nDocumentEntry instance.

### Project overview

![project_overview](src/uml/resources/project_overview.png)

### Examples

SimplI18n offers default implementation for reader and writer.

###### Default Reader

You can defined the ResourceBundle that will be processed

```java
	List<Locale> locales = new ArrayList<>();
	locales.add(Locale.ENGLISH);
	//etc...
	SimplI18nBundleReader reader = new SimplI18nBundleReader("strings", locales); //calling new SimplI18nBundleReader("strings") will use Locale.getDefault()
	I18nDocument document = reader.read();
```

###### Default Java Class writer

You can configure the locales that will be available in the code generation.

```java
	String bundleName = "strings";
	List<Locale> locales = new ArrayList<>();
	locales.add(Locale.ENGLISH);
	//etc...
	SimplI18nJavaWriter writer = new SimplI18nJavaWriter("com.generated", "path/to/save", locales);
	writer.write(document);
```